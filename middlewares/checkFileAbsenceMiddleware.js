const fs = require('fs').promises;

const checkFileAbsenceMiddleware = async (req, res, next) => {
    const {filename: bodyFilename} = req.body;
    const {filename: paramFilename} = req.params;
    const filename = bodyFilename ?? paramFilename;
    const allFiles = await fs.readdir('files');
    const isFileAlreadyExist = allFiles.map((file) => file.toLowerCase()).includes(filename.toLowerCase()); 
    if(!isFileAlreadyExist) {
        return res.status(400).json({message: `No file with '${filename}' filename found`});
    }
    next();
}

module.exports = checkFileAbsenceMiddleware;