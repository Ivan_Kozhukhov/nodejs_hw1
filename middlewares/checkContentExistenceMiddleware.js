const checkContentExistenceMiddleware = (req, res, next) => {
    const {content} = req.body;
    if(!content) {
        return res.status(400).json({message: "Please specify 'content' parameter"});
    }
    next();
} 

module.exports = checkContentExistenceMiddleware;