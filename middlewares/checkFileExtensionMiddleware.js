const checkFileExtention = async (req, _, next) => {
    try {
        const {filename} = req.body;
        const extensionRegex = /\.(?:log|json|js|txt|yaml|xml)$/;
        const isExtentionCorrect = extensionRegex.test(filename);
        if(!isExtentionCorrect) {
            throw new Error('invalid extention');
        }
        next();
    } catch (err) {
        next(err);
    }
}

module.exports = checkFileExtention;