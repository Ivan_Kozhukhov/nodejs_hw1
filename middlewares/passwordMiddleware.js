const fs = require('fs').promises;

const passwordMiddleware = async (req, _, next) => {
    try {
        const {filename} = req.params;
        const {password} = req.query;
        const content = await fs.readFile(`files/${filename}`, 'utf-8');
        const isPasswordExist = content.includes('password=');
        if(isPasswordExist) {
          let filePassword;
          if(password) {
            filePassword = content.split('password=').pop();
          } else {
            throw new Error('You didnt enter the password');
          }
          if(filePassword !== password) {
            throw new Error('Invalid password!');
          }
        }
        next();
    } catch(err) {
        next(err);
    }
} 

module.exports = passwordMiddleware;