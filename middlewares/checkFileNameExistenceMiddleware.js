const checkFileNameExistenceMiddleware = (req, res, next) => {
    const {filename} = req.body;
    if(!filename) {
        return res.status(400).json({message: "Please specify 'filename' parameter"});
    }
    next();
} 

module.exports = checkFileNameExistenceMiddleware;