
const fs = require('fs').promises;

const checkFileExistenceMiddleware = async (req, _, next) => {
    try {
        const {filename: bodyFilename} = req.body;
        const {filename: paramFilename} = req.params;
        const filename = bodyFilename ?? paramFilename;
        const allFiles = await fs.readdir('files');
        const isFileAlreadyExist = allFiles.map((file) => file.toLowerCase()).includes(filename.toLowerCase()); 
        if(isFileAlreadyExist) {
          throw new Error('File is already exist');
        }
        next();
    } catch (err) {
        next(err);
    }
}

module.exports = checkFileExistenceMiddleware;