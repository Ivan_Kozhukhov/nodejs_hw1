const express = require('express');
const router = express.Router();
const { createFile, getFiles, getFile, deleteFile, editFile } = require('./filesService.js');
const checkFileExtension = require('./middlewares/checkFileExtensionMiddleware.js');
const checkFileExistenceMiddleware = require('./middlewares/checkFileExistenceMiddleware.js');
const checkFileAbsenceMiddleware = require('./middlewares/checkFileAbsenceMiddleware.js');
const passwordMiddleware = require('./middlewares/passwordMiddleware.js');
const checkContentExistenceMiddleware = require('./middlewares/checkContentExistenceMiddleware.js');
const checkFileNameExistenceMiddleware = require('./middlewares/checkFileNameExistenceMiddleware.js');

router.post('/', checkFileNameExistenceMiddleware, checkContentExistenceMiddleware, checkFileExistenceMiddleware, checkFileExtension, createFile);

router.get('/', getFiles);

router.get('/:filename', checkFileAbsenceMiddleware, passwordMiddleware, getFile);

router.delete('/:filename', checkFileAbsenceMiddleware, deleteFile);

router.put('/:filename', checkFileAbsenceMiddleware, checkFileExtension, editFile);

module.exports = {
  filesRouter: router
};
