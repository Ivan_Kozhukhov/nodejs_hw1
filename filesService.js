const e = require('express');

const fs = require('fs').promises;

async function createFile (req, res, next) {
  try {
    const {password} = req.query;
    const {filename, content} = req.body;
    const formatedContent = password ? `${content} password=${password}` : content;
    fs.writeFile(`files/${filename}`, formatedContent, 'utf-8');
    res.status(200).send({ "message": "File created successfully" });
  } catch (err) {
    next(err);
  }
}

async function getFiles (_, res, _) {
  const allFiles = await fs.readdir('files');
  res.status(200).send({
    "message": "Success",
    "files": allFiles});
}

const getFile = async (req, res, next) => {
  try {
    const {filename} = req.params;
    const content = await fs.readFile(`files/${filename}`, 'utf-8');
    const extension = filename.split('.').pop();
    const uploadedDate = await (await fs.stat(`files/${filename}`)).birthtime;
    res.status(200).send({
      "message": "Success",
      "filename": filename,
      "content": content.split('password=').shift().trim(),
      "extension": extension,
      "uploadedDate": uploadedDate});
  } catch(err) {
    next(err);
  }
}

const deleteFile = async (req, res, next) => {
  try {
    const {filename} = req.params;
    await fs.unlink(`files/${filename}`);
    res.status(200).json({message: 'File was successfully deleted'});
  } catch (err) {
    next(err);
  }
}

const editFile = async (req, res, next) => {
  try {
    const {filename} = req.params;
    const {filename: newFileName, content} = req.body;
    if(!newFileName && !content) {
      throw new Error("Please specify 'filename' or 'content' parameter")
    }
    if(newFileName && !content) {
      fs.rename(`files/${filename}`, `files/${newFileName}`);
      res.status(200).json({message: `File ${filename} was renamed successfully`});
    } 
    if(content && !newFileName) {
      fs.writeFile(`files/${filename}`, content, 'utf-8');
      res.status(200).json({message: `File ${filename} was changed successfully`});
    }
    fs.rename(`files/${filename}`, `files/${newFileName}`);
    fs.writeFile(`files/${newFileName}`, content, 'utf-8');
    res.status(200).json({message: `File ${filename} was changed and renamed successfully`});
  } catch (err) {
    next(err);
  }
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  deleteFile,
  editFile
}
